package com.fitechsource.test;

import java.util.*;

/**
 * Should be improved to reduce calculation time.
 *
 * Change it or create new one. (max threads count is com.fitechsource.test.TestConsts#MAX_THREADS)
 * Do not use `executors`, only plain threads.
 */
public class Test {

    public static void main(String[] args) throws TestException {
        Set<Double> res = new HashSet<>();

        ThreadsController threadsController = new ThreadsController(TestConsts.MAX_THREADS);
        threadsController.initAndStart(res);
        
        for (int i = 0; i < TestConsts.N; i++) {
            threadsController.offerTask(i);
        }
        
        //offer final signal for threads 
        threadsController.stop();
        
        //wait when threads will be completed.
        threadsController.join();

        System.out.println(res);
        if(threadsController.isErrorObrained())
        {
            System.out.println("The Set<> is not completed. Finished with error.");
        }
    }
}
