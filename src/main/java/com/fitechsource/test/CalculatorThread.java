package com.fitechsource.test;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class CalculatorThread extends Thread {

    public final static Integer LAST_TASK = new Integer(-1);

    // shared set
    private final Collection<Double> result;
    // shared indicator
    private final AtomicBoolean errorIndicator;

    private final BlockingQueue<Integer> blockingQueue;

    public CalculatorThread(Collection<Double> result, BlockingQueue<Integer> blockingQueue,
            AtomicBoolean errorIndicator) {
        this.result = result;
        this.blockingQueue = blockingQueue;
        this.errorIndicator = errorIndicator;
    }

    @Override
    public void run() {
        while (!errorIndicator.get()) {
            Integer task = null;
            try {
                task = blockingQueue.poll(1, TimeUnit.SECONDS);
                if (task == null) {
                    continue;
                }
            } catch (InterruptedException e) {
                errorIndicator.set(true);
            }

            if (errorIndicator.get() || task.equals(LAST_TASK)) {
                break;
            }

            try {
                Set<Double> localResult = TestCalc.calculate(task.intValue());
                storeLocalResult(localResult);
            } catch (TestException testException) {
                errorIndicator.set(true);
            }
        }
    }

    private void storeLocalResult(Set<Double> localResult) {
        synchronized (result) {
            result.addAll(localResult);
        }
    }
}
