package com.fitechsource.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class ThreadsController {
    private final int numThreads;
    private final List<CalculatorThread> threadsPool;
    private final BlockingQueue<Integer> blockingQueue;

    // shared flag between threads.
    private AtomicBoolean errorIndicator = new AtomicBoolean(false);

    public ThreadsController(int numThreads) {
        this.numThreads = numThreads;
        this.threadsPool = new ArrayList<>(numThreads);
        this.blockingQueue = new ArrayBlockingQueue<>(numThreads);
    }

    public void initAndStart(Set<Double> result) {
        for (int i = 0; i < numThreads; i++) {
            CalculatorThread calculatorThread = new CalculatorThread(result, blockingQueue, errorIndicator);
            threadsPool.add(calculatorThread);
            calculatorThread.start();
        }
    }

    public void offerTask(int i) throws TestException {
        Integer task = new Integer(i);
        try {
            while (!errorIndicator.get()) {
                boolean result = blockingQueue.offer(task, 1, TimeUnit.SECONDS);
                if (result == true) {
                    break;
                }
            }
        } catch (InterruptedException e) {
            throw new TestException("InterruptedException catched on blockingQueue.offer()", e);
        }
    }

    /**
     * Finish all working threads.
     */
    public void stop() throws TestException {
        try {
            for (int count = 0; count < numThreads; count++) {
                if (!errorIndicator.get()) {
                    boolean result = blockingQueue.offer(CalculatorThread.LAST_TASK, 1, TimeUnit.SECONDS);
                    if (!result) {
                        continue;
                    }
                }
            }
        } catch (InterruptedException e) {
            throw new TestException("InterruptedException catched on blockingQueue.put()", e);
        }
    }

    /**
     * Join to all threads. Waiting when the threads will be finally stopped.
     */
    public void join() throws TestException {
        for (CalculatorThread thread : this.threadsPool) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new TestException("InterruptedException catched on join()", e);
            }
        }
    }

    public boolean isErrorObrained() {
        return errorIndicator.get();
    }
}
